################
####   AOK  ####
################

AOK_konigsberg_military_aid_fund = {
	
	picture = GFX_decision_cat_picture_aok_military_aid_fund
	
	allowed = {
		original_tag = POL
	}
	
	icon = GFX_decision_cat_icon_aok_military_reserve_fund

	available = {
		GER = { 
		is_in_faction = yes 
		has_government = neutrality 
		}
		is_in_faction_with = GER
	}	
    
	visible = {
		GER = { 
		is_in_faction = yes 
		has_government = neutrality 
		}
		is_in_faction_with = GER
		}
	}

