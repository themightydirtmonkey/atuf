ideas = {
    country = {
    
        SPR_republican_act = {
            
            allowed = {
                
            }
    
            allowed_civil_war = {
                
            }
            
            removal_cost = -1
        
            modifier = {
                stability_factor = -0.20
                political_power_factor = -0.25	
                drift_defence_factor = -0.50
                democratic_drift = 0.02
                democratic_socialism_drift = 0.02
                liberal_drift = 0.03
                communism_drift = 0.015
                neutrality_drift = -0.04
                fascism_drift = 0.01
            }
        }
        
        SPR_his_questionable_mentality = {
        
        allowed = {
                
            }
    
            allowed_civil_war = {
                
            }
            
            removal_cost = -1
            
            modifier = {
                stability_factor = -0.05
            }
    
        }
    }
}