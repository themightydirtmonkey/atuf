POL_historical_plan = {
	name = "Smigly Path"
	desc = "Poland will go down Smigly's political path."

	enable = {
		original_tag = POL
		OR = {
			AND = {
				is_historical_focus_on = yes
				has_game_rule = {
					rule = POL_ai_behavior
					option = DEFAULT
				}
			}
			has_country_flag = POL_AI_RANDOM_HISTORICAL
			has_game_rule = {
				rule = POL_ai_behavior
				option = SMIGLY_PATH
			}
		}
	}
	abort = {
		is_subject = yes
	}

	ai_national_focuses = {
	POL_meeting_of_poznan
	POL_continue_german_alliance
	POL_solve_the_land_disputes
	POL_a_united_common_goal
	POL_alliance_of_war
	POL_forget_the_past_look_towards_the_future
	POL_an_alliance_united
	POL_pilsudskis_passing
	POL_fight_in_the_sejm
	POL_smigly_achieves_victory
	POL_smiglys_address_to_the_nation
	POL_sanacja_ozn_coalition
	POL_a_dictatorship_with_a_dictator
	POL_the_true_successor
	POL_continue_the_cult_of_the_marshal
	POL_the_two_men
	POL_the_streets_run_red_with_communist_blood
	POL_sanacja_paramilitary_group
	POL_unify_the_pilsudskites
	POL_the_next_war_smigly
	POL_smiglys_cult_of_personality
	POL_state_propaganda
	POL_expand_the_state_prisons
	POL_execute_the_endeks
	POL_ensured_internal_stability
	POL_sanacja_ozn_merger_1
	POL_while_we_laugh_they_cry
	POL_rydzs_education_reform
	POL_issue_textbooks
	POL_a_non_rudimentary_education_system
	POL_teachings_of_art_and_architecture
	POL_warsaw_academy_of_architecture
	POL_the_slavic_example
	POL_complete_education_education_reform
	POL_there_is_no_state_without_an_army
	POL_honor_the_legionaries
	POL_enforce_fierce_loyalty
	POL_teachings_of_bravery
	POL_prepare_the_men
	POL_know_the_pain
	POL_death_to_the_enemy
	POL_always_equipped
	POL_champions_of_war
	POL_the_next_war_smigly
	POL_non_discriminatory_conscription
	POL_national_mobilization
	POL_southern_diplomacy_strategy
	POL_support_romanian_claims
	POL_send_aid_to_austria
	POL_the_balkans_greatest_threat
	POL_diplomatic_mission_to_serbia
	POL_diplomatic_mission_to_austria
	POL_strengthen_the_romanian_alliance
	POL_another_enemy
	POL_combat_communism
	POL_czechoslovakia
	POL_spis_or_war
	POL_treaty_of_brno
	POL_invade_hungary
	POL_restore_the_rightful_hungarian_government
	POL_foundations_of_a_new_commonwealth
	POL_fortify_finland
	POL_send_advisors_to_finland
	POL_plan_bombing_in_riga
	POL_border_exercises
	POL_operation_sobieski
	POL_baltic_states_are_ours
	POL_white_russia_is_ours
	POL_ukraine_is_ours
	POL_proclaim_the_commonwealth
	POL_resolve_ethnic_differences
	POL_polanization
	POL_under_our_banner
	POL_continue_kwiatkowskis_ministership
	POL_activate_the_old_polish_industrial_district
	POL_build_up_gydnia
	POL_reduce_inflation
	POL_reform_the_national_bank
	POL_fund_the_port_of_gydnia
	}

	research = {
		infantry_weapons = 50.0
		infantry_tech = 15.0
		artillery = 8.0
		support_tech = 6.5
	}

	ideas = {

	}

	

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
	
	ai_strategy = {
		type = alliance
		id = "GER"
		value = 500
	}
	
	ai_strategy = {
		type = alliance
		id = "MFI"
		value = 500
	}

}