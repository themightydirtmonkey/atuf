focus_tree = {
	id = 1
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = TUR
		}
	}
	#Focus for Constructive Effort
	focus = {
		id = TUR_constructive_effort
		icon = GFX_goal_generic_construction2
		x = 1
		y = 3
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_improve_the_capital }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Industrial Research
	focus = {
		id = TUR_industrial_research
		icon = GFX_goal_generic_scientific_exchange
		x = 5
		y = 2
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_the_five_year_plan }
		search_filters = { FOCUS_FILTER_INDUSTRY } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			add_tech_bonus= {
 bonus = 0.3
 uses = 3 
 category = industry 
}
		}
	}

	#Focus for Unified Industry
	focus = {
		id = TUR_unified_industry
		icon = GFX_focus_generic_industry_3
		x = 2
		y = 5
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_invest_in_eastern_anatolia }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Middle Eastern University
	focus = {
		id = TUR_middle_eastern_university
		icon = GFX_focus_research2
		x = 6
		y = 6
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_technological_improvements }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Revolutionary Industry
	focus = {
		id = TUR_revolutionary_industry
		icon = GFX_TUR_revolutionary_industry-17576
		x = 9
		y = 1
		cost = 10
		available_if_capitulated = yes
		search_filters = { FOCUS_FILTER_INDUSTRY } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			348 = {
add_building_construction = {
    type = industrial_complex
    level = 1
    instant_build = yes
}
}
349 = {
add_building_construction = {
    type = industrial_complex
    level = 1
    instant_build = yes
}
}
355 = {
add_building_construction = {
    type = industrial_complex
    level = 1
    instant_build = yes
}
}
		}
	}

	#Focus for Public Investment
	focus = {
		id = TUR_public_investment
		icon = GFX_goal_generic_construct_infrastructure
		x = 9
		y = 2
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_revolutionary_industry }
		search_filters = { FOCUS_FILTER_INDUSTRY } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			355 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
349 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
354 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
353 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
		}
	}

	#Focus for Improve General Infrastructure
	focus = {
		id = TUR_improve_general_infrastructure
		icon = GFX_goal_generic_construct_infrastructure
		x = 7
		y = 2
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_the_five_year_plan }
		search_filters = { FOCUS_FILTER_INDUSTRY } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			49 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
343 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
346 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
348 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
347 = {
add_building_construction = {
    type = infrastructure
    level = 2
    instant_build = yes
}
}
		}
	}

	#Focus for Technological Improvements
	focus = {
		id = TUR_technological_improvements
		icon = GFX_focus_research2
		x = 6
		y = 5
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_improve_national_industry }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Electronics Focus
	focus = {
		id = TUR_electronics_focus
		icon = GFX_goal_generic_radar
		x = 10
		y = 3
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_public_investment }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Atomic Research
	focus = {
		id = TUR_atomic_research
		icon = GFX_focus_wonderweapons
		x = 10
		y = 4
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_electronics_focus }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Prepare for Central Improvement
	focus = {
		id = TUR_prepare_for_central_improvement
		icon = GFX_TUR_prepare_for_central_improvement-17515
		x = 5
		y = 3
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_improve_general_infrastructure }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Improve National Industry
	focus = {
		id = TUR_improve_national_industry
		icon = GFX_focus_generic_industry_2
		x = 7
		y = 3
		cost = 10
		available_if_capitulated = yes
		prerequisite = { 
			focus = TUR_improve_general_infrastructure 
		}
		prerequisite = { 
			focus = TUR_public_investment 
		}
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Merinos Textile Industry
	focus = {
		id = TUR_merinos_textile_industry
		icon = GFX_TUR_merinos_textile_industry-17520
		x = 4
		y = 5
		cost = 10
		available_if_capitulated = yes
		prerequisite = { 
			focus = TUR_invest_in_eastern_anatolia 
		}
		prerequisite = { 
			focus = TUR_invest_in_central_anatolia 
		}
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Paşabahçe Glass Industry
	focus = {
		id = TUR_paabahe_glass_industry
		icon = GFX_TUR_paabahe_glass_industry-17577
		x = 4
		y = 6
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_merinos_textile_industry }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Invest In Eastern Anatolia
	focus = {
		id = TUR_invest_in_eastern_anatolia
		icon = GFX_TUR_invest_in_eastern_anatolia-17517
		x = 3
		y = 4
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_prepare_for_eastern_improvement }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Invest In Central Anatolia
	focus = {
		id = TUR_invest_in_central_anatolia
		icon = GFX_TUR_invest_in_central_anatolia-17519
		x = 5
		y = 4
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_prepare_for_central_improvement }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Improve the Capital
	focus = {
		id = TUR_improve_the_capital
		icon = GFX_goal_generic_construct_mil_factory
		x = 3
		y = 2
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_the_five_year_plan }
		search_filters = { FOCUS_FILTER_INDUSTRY } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			49 = { 
add_extra_state_shared_building_slots = 1
add_building_construction = {
    type = industrial_complex
    level = 1
    instant_build = yes
}
add_extra_state_shared_building_slots = 1
add_building_construction = {
    type = arms_factory
    level = 1
    instant_build = yes
}
}
		}
	}

	#Focus for The Five Year Plan
	focus = {
		id = TUR_the_five_year_plan
		icon = GFX_TUR_the_five_year_plan-17575
		x = 5
		y = 1
		cost = 10
		available_if_capitulated = yes
		search_filters = { FOCUS_FILTER_INDUSTRY } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			add_tech_bonus= {
 bonus = 1.0
 uses = 1 
 category = industry 
}
		}
	}

	#Focus for Establish Organized Industry
	focus = {
		id = TUR_establish_organized_industry
		icon = GFX_goal_generic_construct_civ_factory
		x = 8
		y = 4
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_improve_national_industry }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for Prepare for Eastern Improvement
	focus = {
		id = TUR_prepare_for_eastern_improvement
		icon = GFX_TUR_prepare_for_eastern_improvement-17516
		x = 3
		y = 3
		cost = 10
		available_if_capitulated = yes
		prerequisite = { focus = TUR_improve_the_capital }
		ai_will_do = {
			factor = 1
		}
	}

	#Focus for test
	focus = {
		id = TUR_test
		icon = GFX_goal_unknown
		x = 1
		y = 15
		cost = 10
		available_if_capitulated = yes
		ai_will_do = {
			factor = 1
		}
	}

#End of focuses 
 }