focus_tree = {
	id = Panama
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = PAN
		}
	}
	default = no

	focus = {
		id = our_darkest_days
		icon = GFX_Focus_Panama_Our_Darkest_Times
		cost = 2.00 
		x = 9
		y = 0
			available_if_capitulated = yes
			completion_reward = {
					add_political_power = 15
				}

	} 
} 
