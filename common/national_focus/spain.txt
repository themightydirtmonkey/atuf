focus_tree = {
	id = spain
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SPR
		}
	}
	default = no
	focus = {
		id = SPR_cabinet_issues
		icon = GFX_SPR_Spanish_Elections
		cost = 10.00
		x = 13
		y = 0
		
	}
	focus = {
		id = SPR_republican_transition
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_cabinet_issues
		}
		x = 0
		y = 1
		relative_position_id = SPR_cabinet_issues
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_overrule_the_republic
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_cabinet_issues
		}
		x = 2
		y = 1
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_iron_fist
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_cabinet_issues
		}
		x = 26
		y = 1
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_throne_claimant
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_iron_fist
		}
		x = 4
		y = 1
		relative_position_id = SPR_iron_fist
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_military_junta
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_iron_fist
		}
		x = 0
		y = 1
		relative_position_id = SPR_iron_fist
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_three_marshals
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_iron_fist
		}
		x = -4
		y = 1
		relative_position_id = SPR_iron_fist
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_crowned_republic
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_republican_transition
		}
		x = 8
		y = 2
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_cut_the_ties
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_republican_transition
		}
		x = 13
		y = 2
		available = {
			always = no
		}
	}
	focus = {
		id = SPR_behead_the_traitor
		icon = GFX_goal_unknown
		cost = 10.00
		prerequisite = {
			focus = SPR_republican_transition
		}
		x = 18
		y = 2
		available = {
			always = no
		}
		bypass = {
		SPR_dead_king
		}
	}
}