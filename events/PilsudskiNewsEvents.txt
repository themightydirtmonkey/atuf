﻿###########################
# News Events
###########################

add_namespace = news

# Poland wins Gdansk
news_event = {
	id = news.337
	title = news.337.title
	desc = news.337.desc
	picture = GFX_news_event_gdansk_handover
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.337.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.337.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.337.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.337.d
		trigger = {
		NOT = {
			TAG = MFI
			TAG = GER
			TAG = POL
		}
	}
	}
}

news_event = {
	id = news.338
	title = news.338.title
	desc = news.338.desc
	picture = GFX_news_event_posen_and_gdansk_handover
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.338.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.338.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.338.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.338.d
		trigger = {
		NOT = {
			TAG = MFI
			TAG = GER
			TAG = POL
		}
	}
	}
}

news_event = { #Pilsudski's Death
	id = news.339
	title = news.339.title
	desc = news.339.desc
	picture = GFX_news_event_pilsudskis_death
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.339.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.339.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.339.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.339.d
		trigger = {
		tag = SOV
	}
	}
	
	option = {
		name = news.339.e
		trigger = {
		NOT = {
		tag = SOV
		tag = MFI
		tag = POL 
		tag = GER
	}
	}
	}
}

news_event = { #Pilsudski's Funeral
	id = news.340
	title = news.340.title
	desc = news.340.desc
	picture = GFX_news_event_pilsudskis_funeral
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.340.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.340.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.340.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.340.d
		trigger = {
		NOT = {
		tag = MFI
		tag = POL 
		tag = GER
	}
	}
	}
}

news_event = { #Pilsudski's Assassination
	id = news.341
	title = news.341.title
	desc = news.341.desc
	picture = GFX_news_event_pilsudski_assassination
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.341.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.341.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.341.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.341.d
		trigger = {
		NOT = {
		tag = MFI
		tag = POL 
		tag = GER
	}
	}
	}
}

news_event = { #Slawek becomes President
	id = news.342
	title = news.342.t
	desc = news.342.desc
	picture = GFX_news_event_president_slawek
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.342.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.342.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.342.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.342.d
		trigger = {
		NOT = {
		tag = MFI
		tag = POL 
		tag = GER
	}
	}
	}
}

news_event = { #Moscicki becomes President
	id = news.343
	title = news.343.title
	desc = news.343.desc
	picture = GFX_news_event_president_moscicki
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.343.a
		trigger = {
		TAG = POL
		}
	}
	
	option = {
		name = news.343.b
		trigger = {
		NOT = {
		tag = POL 
	}
	}
	}
}

news_event = { #Smigly becomes Wodz
	id = news.344
	title = news.344.title
	desc = news.344.desc
	picture = GFX_news_event_marshal_smigly_wodz
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.344.a
		trigger = {
		TAG = POL
		}
	}
	
	option = {
		name = news.344.b
		trigger = {
		TAG = GER
		}
	}
	
	option = {
		name = news.344.c
		trigger = {
		TAG = MFI
		}
	}
	
	option = {
		name = news.344.d
		trigger = {
		TAG = FRA
		}
	}
	
	option = {
		name = news.344.e
		trigger = {
		TAG = ENG
		}
	}
	
	option = {
		name = news.344.f
		trigger = {
		TAG = AUS
		}
	}
	
	option = {
		name = news.344.g
		trigger = {
		TAG = ITA
		}
	}
	
	option = {
		name = news.344.h
		trigger = {
		TAG = SOV
		}
	}
	
	option = {
		name = news.344.i
		trigger = {
		NOT = {
		tag = MFI
		tag = POL 
		tag = GER
		tag = FRA
		tag = ENG
		tag = ITA
		tag = AUS
		tag = SOV
	}
	}
	}
}

news_event = { #Smigly proclaims Commonwealth
	id = news.345
	title = news.345.title
	desc = news.345.desc
	picture = GFX_news_event_commonwealth_proclamation
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.345.a
		trigger = {
		TAG = POL
		}
	}
	
	option = {
		name = news.345.b
		trigger = {
		TAG = GER
		}
	}
	
	option = {
		name = news.345.c
		trigger = {
		TAG = MFI
		}
	}
	
	option = {
		name = news.345.d
		trigger = {
		NOT = {
		tag = MFI
		tag = POL 
		tag = GER
	}
	}
	}
}

news_event = { #Dmowski's Death
	id = news.346
	title = news.346.title
	desc = news.346.desc
	picture = news_event_dmowskis_death
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.346.a
		trigger = {
		TAG = POL
		}
	}
	
	option = {
		name = news.346.b
		trigger = {
		TAG = GER
		}
	}
	
	option = {
		name = news.346.c
		trigger = {
		TAG = MFI
		}
	}
	
	option = {
		name = news.346.d
		trigger = {
		TAG = SOV
	}
	}
	
	option = {
		name = news.346.e
		trigger = {
		TAG = CZE
	}
	}
	
	option = {
		name = news.346.f
		trigger = {
		TAG = FRA
	}
	}
	
	option = {
		name = news.346.g
		trigger = {
		TAG = ENG
	}
	}
	
	option = {
		name = news.346.h
		trigger = {
		NOT = { 
		TAG = POL 
		TAG = GER
		TAG = MFI
		TAG = SOV
		TAG = CZE
		TAG = FRA
		TAG = ENG
		}
	}
	}
}

news_event = { #Kuhlmann-Ciano Pact
	id = news.347
	title = news.347.title
	desc = news.347.desc
	picture = GFX_report_event_germany_kuhlmann_ciano_pact
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.347.a
		trigger = {
		TAG = POL
		}
	}
	
	option = {
		name = news.347.b
		trigger = {
		TAG = GER
		}
	}
	
	option = {
		name = news.347.c
		trigger = {
		TAG = MFI
		}
	}
	
	option = {
		name = news.347.d
		trigger = {
		TAG = SOV
	}
	}
	
	option = {
		name = news.347.e
		trigger = {
		TAG = CZE
	}
	}
	
	option = {
		name = news.347.f
		trigger = {
		TAG = FRA
	}
	}
	
	option = {
		name = news.347.g
		trigger = {
		TAG = ENG
	}
	}
	
	option = {
		name = news.347.h
		trigger = {
		TAG = ITA
	}
	}
	
	option = {
		name = news.347.i
		trigger = {
		NOT = { 
		TAG = POL 
		TAG = GER
		TAG = MFI
		TAG = SOV
		TAG = CZE
		TAG = FRA
		TAG = ENG
		TAG = ITA
		is_in_faction_with = GER
		}
	}
	}
	
	option = {
		name = news.347.j
		trigger = {
		NOT = { 
		TAG = POL
		TAG = MFI
		}
		is_in_faction_with = GER
	}
	}
}

news_event = { #Germany declares Treaty of Versailles Dead
	id = news.348
	title = news.348.title
	desc = news.348.desc
	picture = GFX_news_event_germany_declares_tov_dead
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.348.a
		trigger = {
		TAG = POL
		}
	}
	
	option = {
		name = news.348.b
		trigger = {
		TAG = GER
		}
	}
	
	option = {
		name = news.348.c
		trigger = {
		TAG = MFI
		}
	}
	
	option = {
		name = news.348.d
		trigger = {
		TAG = SOV
	}
	}
	
	option = {
		name = news.348.e
		trigger = {
		TAG = CZE
	}
	}
	
	option = {
		name = news.348.f
		trigger = {
		TAG = FRA
	}
	}
	
	option = {
		name = news.348.g
		trigger = {
		TAG = ENG
	}
	}
	
	option = {
		name = news.348.h
		trigger = {
		TAG = ITA
	}
	}
	
	option = {
		name = news.348.i
		trigger = {
		NOT = { 
		TAG = POL 
		TAG = GER
		TAG = MFI
		TAG = SOV
		TAG = CZE
		TAG = FRA
		TAG = ENG
		TAG = ITA
		is_in_faction_with = GER
		}
	}
	}
	
	option = {
		name = news.348.j
		trigger = {
		NOT = { 
		TAG = POL
		TAG = MFI
		}
		is_in_faction_with = GER
	}
	}
}

# The Fall of Prague (Poland)
news_event = {
	id = news.349
	title = news.349.title
	desc = news.349.desc
	picture = GFX_news_event_poland_captures_prague
	
	major = yes
	
	trigger = {
		9 = { is_controlled_by = POL }
		9 = { is_owned_by = CZE }
		POL = { has_war_with = CZE }
		NOT = { has_global_flag = fall_of_prague_pol }
	}
	
	immediate = {
		set_global_flag = fall_of_prague_pol
	}
	
	mean_time_to_happen = {
		days = 2
	}
	
	option = {
		name = news.349.a
		trigger = {
			NOT = {
				TAG = POL
				TAG = CZE
				is_in_faction_with = POL
				is_in_faction_with = CZE
			}
		}
	}
	option = {
		name = news.349.b
		trigger = { TAG = POL }
	}
	option = {
		name = news.349.c
		trigger = { TAG = CZE }
	}
	option = {
		name = news.349.d
		trigger = { 
		is_in_faction_with = POL
		NOT = { TAG = POL }
		}
	}
	option = {
		name = news.349.e
		trigger = { 
		is_in_faction_with = CZE
		}
	}
}

# The Fall of Kiev (Poland)
news_event = {
	id = news.350
	title = news.350.t
	desc = news.350.desc
	picture = GFX_news_event_poland_captures_kiev
	
	major = yes
	
	trigger = {
		202 = { is_controlled_by = POL }
		202 = { is_owned_by = SOV }
		POL = { has_war_with = SOV }
		NOT = { has_global_flag = fall_of_kiev_pol }
	}
	
	immediate = {
		set_global_flag = fall_of_kiev_pol
	}
	
	mean_time_to_happen = {
		days = 2
	}
	
	option = {
		name = news.350.a
		trigger = {
			NOT = {
				TAG = POL
				TAG = SOV
				is_in_faction_with = POL
				is_in_faction_with = SOV
			}
		}
	}
	option = {
		name = news.350.b
		trigger = { TAG = POL }
	}
	option = {
		name = news.350.c
		trigger = { TAG = SOV }
	}
	option = {
		name = news.350.d
		trigger = { 
		is_in_faction_with = POL
		NOT = { TAG = POL }
		}
	}
	option = {
		name = news.350.e
		trigger = { 
		is_in_faction_with = SOV
		}
	}
}

# The Fall of Moscow (Poland)
news_event = {
	id = news.351
	title = news.351.t
	desc = news.351.desc
	picture = GFX_news_event_poland_captures_moscow
	
	major = yes
	
	trigger = {
		219 = { is_controlled_by = POL }
		219 = { is_owned_by = SOV }
		POL = { has_war_with = SOV }
		NOT = { has_global_flag = fall_of_moscow_pol }
	}
	
	immediate = {
		set_global_flag = fall_of_moscow_pol
	}
	
	mean_time_to_happen = {
		days = 2
	}
	
	option = {
		name = news.351.a
		trigger = {
			NOT = {
				TAG = POL
				TAG = SOV
				is_in_faction_with = POL
				is_in_faction_with = SOV
			}
		}
	}
	option = {
		name = news.351.b
		trigger = { TAG = POL }
	}
	option = {
		name = news.351.c
		trigger = { TAG = SOV }
	}
	option = {
		name = news.351.d
		trigger = { 
		is_in_faction_with = POL
		NOT = { TAG = POL }
		}
	}
	option = {
		name = news.351.e
		trigger = { 
		is_in_faction_with = SOV
		}
	}
}

# The Fall of Stalingrad (Poland)
news_event = {
	id = news.352
	title = news.352.t
	desc = news.352.desc
	picture = GFX_news_event_poland_captures_stalingrad
	
	major = yes
	
	trigger = {
		217 = { is_controlled_by = POL }
		217 = { is_owned_by = SOV }
		POL = { has_war_with = SOV }
		NOT = { has_global_flag = fall_of_stalingrad_pol }
	}
	
	immediate = {
		set_global_flag = fall_of_stalingrad_pol
	}
	
	mean_time_to_happen = {
		days = 2
	}
	
	option = {
		name = news.352.a
		trigger = {
			NOT = {
				TAG = POL
				TAG = SOV
				is_in_faction_with = POL
				is_in_faction_with = SOV
			}
		}
	}
	option = {
		name = news.352.b
		trigger = { TAG = POL }
	}
	option = {
		name = news.352.c
		trigger = { TAG = SOV }
	}
	option = {
		name = news.352.d
		trigger = { 
		is_in_faction_with = POL
		NOT = { TAG = POL }
		}
	}
	option = {
		name = news.352.e
		trigger = { 
		is_in_faction_with = SOV
		}
	}
}

#Wilhelm III becomes Kaiser
news_event = {
	id = news.353
	title = news.353.title
	desc = news.353.desc
	picture = GFX_news_event_gdansk_handover
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.353.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.353.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.353.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.353.d
		trigger = {
		NOT = {
			TAG = MFI
			TAG = GER
			TAG = POL
			TAG = ENG
			TAG = FRA
		}
	}
	}
	
	option = {
		name = news.353.e
		trigger = {
		TAG = ENG
	}
	}
	
	option = {
		name = news.353.f
		trigger = {
		TAG = FRA
	}
	}
}

#Austrian Referendum
news_event = {
	id = news.354
	title = news.354.title
	desc = news.354.desc
	picture = GFX_news_event_austria_referendum_junta
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.354.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.354.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.354.c
		trigger = {
		NOT = {
			TAG = GER
			TAG = POL
			TAG = ENG
			TAG = FRA
		}
	}
	}
	
	option = {
		name = news.354.e
		trigger = {
		TAG = ENG
	}
	}
	
	option = {
		name = news.354.f
		trigger = {
		TAG = FRA
	}
	}
}

#Austrian Submission Germany Unify
news_event = {
	id = news.355
	title = news.355.title
	desc = news.355.desc
	picture = GFX_news_event_government_submission
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.355.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.355.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.355.c
		trigger = {
		NOT = {
			TAG = GER
			TAG = POL
			TAG = ENG
			TAG = FRA
		}
	}
	}
	
	option = {
		name = news.355.d
		trigger = {
		TAG = ENG
	}
	}
	
	option = {
		name = news.355.e
		trigger = {
		TAG = FRA
	}
	}
}

#Germany Abandon AoK
news_event = {
	id = news.356
	title = news.356.title
	desc = news.356.desc
	picture = GFX_news_event_germany_abandon_AoK
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	option = {
		name = news.356.a
		trigger = {
		TAG = POL
		}
	}
	option = {
		name = news.356.b
		trigger = {
			TAG = GER
		}
	}
	
	option = {
		name = news.356.c
		trigger = {
			TAG = MFI
		}
	}
	
	option = {
		name = news.356.d
		trigger = {
		NOT = {
			TAG = MFI
			TAG = GER
			TAG = POL
			TAG = ENG
			TAG = FRA
		}
	}
	}
	
	option = {
		name = news.355.e
		trigger = {
		TAG = ENG
	}
	}
	
	option = {
		name = news.355.f
		trigger = {
		TAG = FRA
	}
	}
}


