﻿capital = 10

oob = "POL_1936"
if = {
	limit = { has_dlc = "Man the Guns" }

	# Submarines #
	create_equipment_variant = {
		name = "Wilk Class"				
		type = ship_hull_submarine_2
		name_group = POL_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = ship_mine_layer_sub
		}
	}
	# Destroyers #
	create_equipment_variant = {
		name = "Wicher Class"				
		type = ship_hull_light_1
		name_group = POL_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_mine_layer_1
			rear_1_custom_slot = ship_depth_charge_1
		}
	}
	#Light Cruiser #

	create_equipment_variant = {
		name = "Warszawa Class"				
		type = ship_hull_cruiser_1
		name_group = POL_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			mid_1_custom_slot =  ship_light_battery_1
			mid_2_custom_slot =  ship_light_medium_battery_1
			rear_1_custom_slot = ship_medium_battery_1
			fixed_ship_armor_slot = empty
		}
	}

}
set_research_slots = 4

add_ideas = {
pilsudski_cult
master_of_the_slavs
german_alliance_issue 
threat_of_russia
dysfunctional_military
pol_great_depression
#Mob Law
low_economic_mobilisation
#Trade Law
free_trade
#Ministers
#Head of Government
POL_jozef_pilsudski
#Deputy Head
POL_waclaw_jedrzejewicz
#Treasury
POL_eugeniusz_kwiatowski_1
#Justice
POL_emil_rappaport
#Interior
POL_bronislaw_pieracki
#War
POL_kazimierz_sosnkowski_war
}
# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_mountaineers = 1
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	gw_artillery = 1
	interwar_antiair = 1
	gwtank = 1
	early_fighter = 1
	early_bomber = 1
	CAS1 = 1
	trench_warfare = 1
	basic_light_tank = 1
	improved_light_tank = 1
	motorised_infantry = 1
}
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	set_technology = {
		early_destroyer = 1
		early_submarine = 1	
	}
}
if = {
	limit = { has_dlc = "Man the Guns" }
	set_technology = {
		basic_naval_mines = 1
		submarine_mine_laying = 1
		early_ship_hull_light = 1
		early_ship_hull_submarine = 1
		basic_ship_hull_submarine = 1
		basic_battery = 1
		basic_torpedo = 1
		basic_depth_charges = 1
	}
}


set_convoys = 30
pro_german_alliance = yes
anti_german_alliance = yes
smigly_support = yes
slawek_support = yes
moscicki_support = yes
nat_democrat_favor = yes
commie_favor = yes

set_politics = {
	ruling_party = neutrality
	last_election = "1935.9.8"
	election_frequency = 36
	elections_allowed = no
}
set_popularities = {
	democratic = 7
	fascism = 5
	communism = 2
	neutrality = 86
}
add_opinion_modifier = {
    target = FIN
    modifier = nonrecognized_government
}
add_opinion_modifier = {
    target = SOV
    modifier = historical_grievences
}

add_manpower = 178965
set_major = yes

create_country_leader = {
	name = "Józef Piłsudski"
	desc = "POLITICS_JOZEF_PILSUDSKI_DESC"
	picture = "Portrait_Poland_Jozef_Pilsudski.dds"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		father_of_the_nation
		marshal_of_poland
	}
}

create_country_leader = {
	name = "Roman Dmowski"
	desc = "POLITICS_ROMAN_DMOWSKI_DESC"
	picture = "Portrait_Poland_Roman_Dmowski.dds"
	expire = "1965.1.1"
	ideology = conservatism
	traits = {
	father_of_polish_nationalism	
	}
}

create_country_leader = {
	name = "Adolf Warski"
	desc = "POLITICS_ROMAN_DMOWSKI_DESC"
	picture = "Portrait_POL_Adolf_Warski.dds"
	expire = "1965.1.1"
	ideology = marxism
	traits = {
		
	}
}

add_ai_strategy = {
    type = alliance
    id = GER
    value = 300
}


create_field_marshal = {
	name = "Józef Piłsudski"
	picture = "Portrait_Poland_Jozef_Pilsudski.dds"
	traits = { thorough_planner offensive_doctrine old_guard politically_connected }
	skill = 4
	attack_skill = 5
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 2
	id = 1200
}
create_field_marshal = {
	name = "Władysław Sikorski"
	picture = "Portrait_Poland_Wladysaw_Sikorski_Pilsudski.dds"
	traits = { defensive_doctrine old_guard politically_connected }
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 3
}

create_field_marshal = {
	name = "Edward Rydz-Śmigły"
	picture = "Portrait_Poland_Edward_Rydz_Smigly.dds"
	traits = { offensive_doctrine politically_connected }
	skill = 4
	attack_skill = 4
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
	id = 1000
}

create_field_marshal = {
	name = "Tadeusz Bór-Komorowski"
	picture = "Portrait_Poland_Tadeusz_Komorowski.dds"
	traits = { logistics_wizard cavalry_officer war_hero }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 4
}

create_field_marshal = {
	name = "Kazimierz Sosnkowski"
	picture = "Portrait_POL_Kazimierz_Sosnkowski.dds"
	traits = { defensive_doctrine engineer_trait old_guard }
	skill = 3
	attack_skill = 2
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Stanisław Kopański"
	picture = "Portrait_Poland_Stanislaw_Kopanski.dds"
	traits = { trait_engineer brilliant_strategist }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 2
}

create_corps_commander = {
	name = "Józef Haller von Hallenburg"
	picture = "Portrait_POL_Jozef_Haller.dds"
	traits = { old_guard war_hero politically_connected brilliant_strategist }
	skill = 4
	id = 1250
	attack_skill = 4
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Juliusz Rómmel"
	picture = "Portrait_Poland_Juliusz_Rommel.dds"
	traits = { war_hero career_officer }
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 1
}

create_corps_commander = {
	name = "Władysław Anders"
	picture = "Portrait_Poland_Wladyslaw_Anders.dds"
	traits = {  armor_officer brilliant_strategist }
	skill = 4
	attack_skill = 4
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Stanisław Sosabowski"
	picture = "Portrait_Poland_Stanislaw_Sosabowski.dds"
	traits = {  commando infantry_officer skilled_staffer }
	skill = 4
	attack_skill = 4
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 4
}

create_corps_commander = {
	name = "Roman Abraham"
	picture = "Portrait_Poland_Roman_Abraham.dds"
	traits = { cavalry_officer }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Wincenty Kowalski"
	picture = "Portrait_Poland_Wincenty_Kowalski.dds"
	traits = { trickster infantry_officer brilliant_strategist }
	skill = 4
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}

create_corps_commander = {
	name = "Stefan Rowecki"
	picture = "Portrait_Poland_Stefan_Pawe_Rowecki.dds"
	traits = { combined_arms_expert war_hero cavalry_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 3
}

create_corps_commander = {
	name = "Bolesław Wieniawa-Długoszowski"
	picture = "Portrait_Poland_Boleslaw_Dlugoszowski.dds"
	traits = { politically_connected war_hero cavalry_officer }
	skill = 4
	attack_skill = 4
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Stefan Dąb-Biernacki"
	picture = "Portrait_Poland_Stefan_Dab_Biernacki.dds"
	traits = { trait_reckless harsh_leader old_guard }
	skill = 2
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}

create_corps_commander = {
	name = "Bolesław Bronisław Duch"
	picture = "Focus_Poland_Boleslaw_Duch.dds"
	traits = { infantry_officer inflexible_strategist }
	skill = 2
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 2
}

create_navy_leader = {
	name = "Józef Unrug"
	picture = "Portrait_Poland_Jozef_Unrug.dds"
	traits = { green_water_expert lone_wolf }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 4
	coordination_skill = 2
}

create_navy_leader = {
	name = "Jerzy Świrski"
	picture = "Portrait_Poland_Jerzy_Swirski.dds"
	traits = { green_water_expert }
	skill = 2
	attack_skill = 2
	defense_skill = 3
	maneuvering_skill = 2
	coordination_skill = 1
}



create_equipment_variant = {
	name = "PZL P.24"
	type = fighter_equipment_0
	upgrades = {
		plane_gun_upgrade = 3
		plane_range_upgrade = 0  
		plane_engine_upgrade = 1
		plane_reliability_upgrade = 3
	}
}

### VARIANTS ###
# 1936 Start #
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	### Ship Variants ###
}
if = {
	limit = { has_dlc = "Man the Guns" }
	# Submarines #
	create_equipment_variant = {
		name = "Wilk Class"				
		type = ship_hull_submarine_2
		name_group = POL_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = ship_mine_layer_sub
		}
	}
	# Destroyers #
	create_equipment_variant = {
		name = "Wicher Class"				
		type = ship_hull_light_1
		name_group = POL_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_mine_layer_1
			rear_1_custom_slot = ship_depth_charge_1
		}
	}
}


# 1939 Start #
1939.1.1 = {
	if = {
		limit = { not = { has_dlc = "Man the Guns" } }
		# Ship variants #
	}
	if = {
		limit = { has_dlc = "Man the Guns" }
		# Submarines #
		create_equipment_variant = {
			name = "Orzel Class"				
			type = ship_hull_submarine_2
			name_group = POL_SS_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_torpedo_slot = ship_torpedo_sub_2
				fixed_ship_engine_slot = sub_ship_engine_2
				rear_1_custom_slot = ship_torpedo_sub_2
			}
		}
		# Destroyers #
		create_equipment_variant = {
			name = "Grom Class"					
			type = ship_hull_light_2
			name_group = POL_DD_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_2
				fixed_ship_torpedo_slot = ship_torpedo_1
				mid_1_custom_slot = ship_mine_layer_1
				rear_1_custom_slot = ship_depth_charge_1
			}
		}
		create_equipment_variant = {
			name = "Gryf Class"						# minelaying craft
			type = ship_hull_light_2
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_light_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = light_ship_engine_1
				fixed_ship_torpedo_slot = empty
				mid_1_custom_slot = ship_mine_layer_1
				rear_1_custom_slot = ship_mine_layer_1
			}
		}
	}
}