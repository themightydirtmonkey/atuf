﻿capital = 105 # Montenegro

oob = "MNT_1936"

set_research_slots = 2

set_technology = {
	tech_support = 1		
	tech_engineers = 1
	tech_mountaineers = 1
	infantry_weapons = 1
	
}

starting_ideas = {
mnt_north_region

}


set_popularities = {
	neutrality = 55
	fascism = 20
	communism = 25
}

set_politics = {
	ruling_party = neutrality
	last_election = "1935.9.8"
	election_frequency = 36
	elections_allowed = no
}


create_country_leader = {
	name = "Sekula Drljević"
	
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
	expire = "1953.3.1"
	ideology = fascism_ideology
	traits = {
		
	}
}

create_country_leader = {
	name = "Xenia Petrović-Njegoš"
	
	picture = "Portrait_Montenegro_Xenia_Petrovic-Njegos.dds"
	expire = "1953.3.1"
	ideology = monarchist
	traits = {
	Montenegro_queen
	Balkan_Nymph 
	}
}

create_country_leader = {
	name = "Blažo Jovanović"
	
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_land_5.dds"
	expire = "1953.3.1"
	ideology = leninism
	traits = {
		
	}
}


