﻿division_template = {
	name = "Brigada na kralicata"			# Infantry Division

	division_names_group = MON_CAV_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
	}
	support = {
	recon = { x = 0 y = 0 }
}
}

units = {
	division= {	
		name = "1. Brigada na kralicata"
		location = 6886
		division_template = "Brigada na kralicata"
		start_experience_factor = 0.2
	}