﻿division_template = {
	name = "Polish Royal Guard"		
	division_names_group = POL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		artillery = { x = 1 y = 3 }
	}
	support = {
        recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
}


units = {
	division= {	
		name = "1. Józef Poniatowski Legion" 
		location = 3544
		division_template = "Polish Royal Guard"
		start_experience_factor = 0.7
	}
	division= {	
		name = "2. Vistula Legion" 
		location = 3544
		division_template = "Polish Royal Guard"
		start_experience_factor = 0.7
	}
	division= {	
		name = "3. Jan Henryk Dąbrowski Legion" 
		location = 3544
		division_template = "Polish Royal Guard"
		start_experience_factor = 0.7
	}
}