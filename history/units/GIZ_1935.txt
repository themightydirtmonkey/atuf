﻿division_template = {
	name = "Revolutionary Militia"			# Infantry Division

	#division_names_group = 

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
	}
	support = {
}
}

units = {
	division= {	
		name = "First Brigade"
		location = 4135
		division_template = "Revolutionary Militia"
		start_experience_factor = 0.6
	}
	
	division= {	
		name = "Liberty Brigade"
		location = 4135
		division_template = "Revolutionary Militia"
		start_experience_factor = 0.6
	}
	
	division= {	
		name = "Justice Brigade"
		location = 13228
		division_template = "Revolutionary Militia"
		start_experience_factor = 0.6
	}
}