﻿division_template = {
	name = "Dywizja Piechoty"		
	division_names_group = POL_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
        recon = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Dywizja Piechoty Górskiej"
	division_names_group = POL_MNT_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }	
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 2 y = 0 }	
		mountaineers = { x = 2 y = 1 }	
		mountaineers = { x = 2 y = 2 }	
	}
	support = {
        recon = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Brygada Kawalerii" 
	division_names_group = POL_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
	}
	support = {
		recon = { x = 0 y = 0 }     
	}
}


units = {
	######## Sztab Generalny Wojska Polskiego ########
	### Armia Pomorze ###	
	division= {	# "4 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 11301
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "15 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 15
		}
		location = 11467
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "16 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 16
		}
		location = 9263
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}

	### Armia Poznan ###	
	division= {	# "14 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}
		location = 6558
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "31 Dywizja Piechoty"
		name = "31 Dywizja Piechoty" 
		location = 6558
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6

	}
	division= {	# "32 Dywizja Piechoty"
		name = "32 Dywizja Piechoty" 
		location = 279
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7

	}
	division= {	# "33 Dywizja Piechoty"
		name = "33 Dywizja Piechoty" 
		location = 279
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7

	}
	division= {	# "34 Dywizja Piechoty"
		name = "34 Dywizja Piechoty" 
		location = 9263
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "35 Dywizja Piechoty"
		name = "35 Dywizja Piechoty" 
		location = 9452
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "36 Dywizja Piechoty"
		name = "36 Dywizja Piechoty" 
		location = 3230
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "37 Dywizja Piechoty"
		name = "37 Dywizja Piechoty" 
		location = 11357
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "38 Dywizja Piechoty"
		name = "38 Dywizja Piechoty" 
		location = 6484
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "39 Dywizja Piechoty"
		name = "39 Dywizja Piechoty" 
		location = 417
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "40 Dywizja Piechoty"
		name = "40 Dywizja Piechoty" 
		location = 6604
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "41 Dywizja Piechoty"
		name = "41 Dywizja Piechoty" 
		location = 11515
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.5

	}
	division= {	# "42 Dywizja Piechoty"
		name = "42 Dywizja Piechoty" 
		location = 9494
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.4
		start_equipment_factor = 0.7

	}
	division= {	# "43 Dywizja Piechoty"
		name = "43 Dywizja Piechoty" 
		location = 584
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.4
		start_equipment_factor = 0.7

	}
	division= {	# "17 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}
		location = 11301
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "25 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 25
		}
		location = 6522
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Podolska B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 106
		}
		location = 6522
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Wielkopolska B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 109
		}
		location = 442
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}

	### Armia Lódz ###	
	division= {	# "2 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 9508
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "7 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 9508
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "10 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 3458
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "Kresowa B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 102
		}
		location = 402
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Wolynska B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 111
		}
		location = 402
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}

	### Armia Kraków ###	
	division= {	# "6 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 9427
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "21 Dywizja Piechoty Górskiej"
		division_name = {
			is_name_ordered = yes
			name_order = 21
		}
		location = 3458
		division_template = "Dywizja Piechoty Górskiej"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "23 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 23
		}
		location = 9412
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Krakowska B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 101
		}
		location = 9427
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}

	### Armia Modlin ###	
	division= {	# "8 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 3544
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "26 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 26
		}
		location = 3544
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "28 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 28
		}
		location = 3544
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Nowogródzka B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 104
		}
		location = 11385
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Mazowiecka B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 103
		}
		location = 11385
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}

	## GO Lublin ##	
	division= {	# "9 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 11399
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "30 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 30
		}
		location = 11399
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}

	## SGO Narew ##	
	division= {	# "18 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 18
		}
		location = 290
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.7

	}
	division= {	# "29 Dywizja Piechoty"
		division_name = {
			is_name_ordered = yes
			name_order = 29
		}
		location = 290
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Suwalska B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 108
		}
		location = 290
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	# "Pomorska B.K."
		division_name = {
			is_name_ordered = yes
			name_order = 107
		}
		location = 290
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_0 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
}

##### Wojska Lotnicze i Obrony Powietrzne #####
air_wings = {
	10 = { 
		fighter_equipment_0 =  {
			owner = "POL" 
			amount = 108
		}
		
	}
	86 = { 
		fighter_equipment_0 =  {
			owner = "POL" 
			amount = 54
		}
		CAS_equipment_1 =  {
			owner = "POL" 
			amount = 22
		}
	}
}

#########################
## STARTING PRODUCTION ##
#########################
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "POL"
		}
		requested_factories = 2
		progress = 0.20
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "POL"
		}
		requested_factories = 1
		progress = 0.20
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = fighter_equipment_0
			creator = "POL"
		}
		requested_factories = 1
		progress = 0.20
		efficiency = 100
	}
}