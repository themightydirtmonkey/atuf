units = {
	fleet = {
		name = "Soviet Destroyer Squadron"
		naval_base = 3686 # Sevastopopl
		task_force = {					
			name = "Soviet Destroyer Flotilla 1"				
			location = 3686 # 			
			ship = { name = "HUN Budapest" definition = destroyer equipment = { ship_hull_light_1 = { amount = 1 owner = HUN version_name = "A/B/C/D Class" } } }				
			ship = { name = "HUN Béla Kun"  definition = destroyer equipment = { ship_hull_light_1 = { amount = 1 owner = HUN version_name = "A/B/C/D Class" } } }				
						
	}
}	