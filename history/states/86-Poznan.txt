state={
	id=86
	name="STATE_86"
	manpower = 1856000
	
	
	state_category = city
	history={
		owner = POL
		victory_points = {
			6558 10
		}
		victory_points = {
			11558 4
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			arms_factory = 1
			11232 = {
				bunker = 1

			}
			9532 = {
				bunker = 1

			}
			3381 = {
				bunker = 1

			}
			air_base = 5
		}
		add_core_of = POL
	}

	provinces={
		17 3381 6558 9532 11232 11558
	}
}
