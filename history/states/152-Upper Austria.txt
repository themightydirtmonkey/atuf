
state={
	id=152
	name="STATE_152"
	resources={
		steel=5.000
		aluminium=1.000
	}

	history={
		owner = AUS
		buildings = {
			infrastructure = 6
			industrial_complex = 3

		}
		victory_points = {
			688 10 
		}
		victory_points = {
			3673 3 
		}
		victory_points = {
			9665 3 
		}
		victory_points = {
			732 10 
		}
		add_core_of = AUS
		1938.3.12 = {
			owner = GER
			controller = GER
			add_core_of = GER

		}

	}

	provinces={
		732 3703 6708 9665 
	}
	manpower=275377
	buildings_max_level_factor=1.000
	state_category=city
}
