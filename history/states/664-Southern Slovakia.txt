
state={
	id=664
	name="STATE_664"
	resources={
		steel=13.000
		chromium=3.000
	}

	history={
		owner = CZE
		buildings = {
			infrastructure = 6
			industrial_complex = 2

		}
		victory_points = {
			6573 1 
		}
		add_core_of = CZE
		add_core_of = HUN
	}

	provinces={
		3716 6561 11679 
	}
	manpower=675891
	buildings_max_level_factor=1.000
	state_category=town
}
