state={
	id=818
	name="STATE_818"
	history={
		owner = GER
		victory_points = {
			3530 10
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			air_base = 2

		}
		add_core_of = GER
		add_core_of = BAD

	}
	provinces={
		3530 6542 6712 11640 
	}
	manpower=1954793
	state_category = town
	buildings_max_level_factor=1.000
}
