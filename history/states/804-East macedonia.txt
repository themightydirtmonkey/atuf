
state={
	id=804
	name="STATE_804"
	resources={
		chromium=35.000
		aluminium=8.000
	}

	history={
		owner = BMC
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			arms_factory = 1
			air_base = 2

		}
		victory_points = {
			3882 5 
		}
		add_core_of = MAC
		add_core_of = BMC

	}

	provinces={
		907 3833 6886 
	}
	manpower=687100
	buildings_max_level_factor=1.000
	state_category=town
}
